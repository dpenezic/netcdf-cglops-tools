FROM python:3.4

RUN useradd --create-home --shell /bin/bash user

ENV PATH=${PATH}:/home/user/.local/bin

WORKDIR /home/user

COPY . .

RUN apt-get update \
    && apt-get install -y cdo

RUN pip install --requirement requirements/production.txt \
    && pip install --editable .

USER user
