# -*- encoding: utf-8-*-

import pytest

from netcdf_cglops_tools import netcdfanalyzer

pytestmark = pytest.mark.unit


@pytest.mark.parametrize(
    "contact_data, number_of_parsed_contacts, pattern_to_use", [
    ([
        {
            "name": "Instituto Português do Mar e da Atmosfera",
            "short_name": "IPMA",
            "position": "Researcher",
            "delivery_point": "Rua C ao Aeroporto",
            "city": "Lisbon",
            "postal_code": "1749-077",
            "country": "Portugal",
            "country_code": "PT",
            "e_mail": "isabel.trigo@ipma.pt",
            "website": "http://www.ipma.pt",
            "role": "Principal investigator",
        },
        {
            "name": "Instituto Português do Mar e da Atmosfera",
            "short_name": "IPMA",
            "position": "IPMA GIO-Global Land Help Desk",
            "delivery_point": "Rua C ao Aeroporto",
            "city": "Lisbon",
            "postal_code": "1749-077",
            "country": "Portugal",
            "country_code": "PT",
            "e_mail": "sandra.coelho@ipma.pt",
            "website": "http://www.ipma.pt",
            "role": "Originator",
        },
        {
            "name": "Flemish Institute for Technological Research",
            "short_name": "VITO",
            "position": "GIO-Global Land Help Desk",
            "delivery_point": "Boeretang 200",
            "city": "Mol",
            "postal_code": "2400",
            "country": "Belgium",
            "country_code": "BE",
            "e_mail": "helpdesk@vgt.vito.be",
            "website": "http://land.copernicus.eu/global/",
            "role": "Point of contact",
        },
        {
            "name": "European Commission Directorate-General for Enterprise "
                    "and Industry",
            "position": "Owner",
            "short_name": "EC-DGEI",
            "delivery_point": "Avenue d'Auderghem 45",
            "city": "Brussels",
            "postal_code": "1049",
            "country": "Belgium",
            "country_code": "BE",
            "e_mail": "ENTR-COPERNICUS-ASSETS@ec.europa.eu",
            "website": "http://ec.europa.eu/enterprise/",
            "role": "Owner",
        },
        {
            "name": "European Commission Directorate-General Joint "
                    "Research Center",
            "short_name": "JRC",
            "position": "Responsible",
            "delivery_point": "Via E.Fermi, 249",
            "city": "Ispra",
            "postal_code": "21027",
            "country": "Italy",
            "country_code": "IT",
            "e_mail": "copernicuslandproducts@jrc.ec.europa.eu",
            "website": "http://ies.jrc.ec.europa.eu",
            "role": "Custodian",
        },
    ], 5, "correct"),
    ([], 0, "correct"),
    ([
         {
             "name": "phony",
             "short_name": "P",
             "position": "Joker",
             "delivery_point": "neverland",
             "city": "arouca",
             "postal_code": "11111",
             "country": "Finland",
             "country_code": "IT",
             "e_mail": "nonone@nomail",
             "website": "http://fake.com",
             "role": "roll over",
         }
     ], 1, "correct"),
    ([
         {
             "name": "Instituto Português do Mar e da Atmosfera",
             "short_name": "IPMA",
             "position": "Researcher",
             "delivery_point": "Rua C ao Aeroporto",
             "city": "Lisbon",
             "postal_code": "1749-077",
             "country": "Portugal",
             "country_code": "PT",
             "e_mail": "isabel.trigo@ipma.pt",
             "website": "http://www.ipma.pt",
             "role": "Principal investigator",
         },
         {
             "name": "Instituto Português do Mar e da Atmosfera",
             "short_name": "IPMA",
             "position": "IPMA GIO-Global Land Help Desk",
             "delivery_point": "Rua C ao Aeroporto",
             "city": "Lisbon",
             "postal_code": "1749-077",
             "country": "Portugal",
             "country_code": "PT",
             "e_mail": "sandra.coelho@ipma.pt",
             "website": "http://www.ipma.pt",
             "role": "Originator",
         },
     ], 0, "wrong")
])
def test_extract_contact_details(contact_data, number_of_parsed_contacts,
                                 pattern_to_use):
    correct_pattern = (
        "{role} ({position}): {e_mail}; {name} ({short_name}); "
        "{delivery_point}; {city}; {postal_code}; "
        "{country} ({country_code}); IPMA website; {website}\n"
    )
    # note the missing semicolon between `short_name` and `delivery_point`
    # parameters in this pattern
    wrong_pattern = (
        "{role} ({position}): {e_mail}; {name} ({short_name}) "
        "{delivery_point}; {city}; {postal_code}; "
        "{country} ({country_code}); VITO website; {website}\n "
    )

    pattern = {
        "correct": correct_pattern,
        "wrong": wrong_pattern
    }.get(pattern_to_use)
    contact_expression = ""
    for contact in contact_data:
        contact_expression += pattern.format(**contact)
    result = netcdfanalyzer.ProductInfo.extract_contact_details(
        contact_expression)
    assert len(result) == number_of_parsed_contacts
    for index in range(len(result)):
        assert result[index] == netcdfanalyzer.ContactDetails(
            name=contact_data[index]["name"],
            short_name=contact_data[index]["short_name"],
            position=contact_data[index]["position"],
            delivery_point=contact_data[index]["delivery_point"],
            city=contact_data[index]["city"],
            postal_code=contact_data[index]["postal_code"],
            country=contact_data[index]["country"],
            country_code=contact_data[index]["country_code"],
            e_mail=contact_data[index]["e_mail"],
            website=contact_data[index]["website"],
            role=contact_data[index]["role"],
        )
