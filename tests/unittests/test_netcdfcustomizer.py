import mock
import os
import pytest

from netcdf_cglops_tools import netcdfcustomizer

pytestmark = pytest.mark.unit


class TestNetcdfContextManager(object):

    def test_initialization(self):
        fake_path = "no/path/here"
        fake_mode = "phony"
        fake_format = "fake"
        manager = netcdfcustomizer.NetcdfContextManager(
            fake_path,
            mode=fake_mode,
            format_=fake_format
        )
        assert manager.path == fake_path
        assert manager.mode == fake_mode
        assert manager.format_ == fake_format
        assert manager.dataset is None

    def test_enter(self):
        fake_path = "no/path/here"
        fake_mode = "phony"
        fake_format = "fake"
        manager = netcdfcustomizer.NetcdfContextManager(
            fake_path, mode=fake_mode, format_=fake_format)
        with mock.patch("netcdf_cglops_tools.netcdfcustomizer.netCDF4",
                        autospec=True) as mock_netcdf4:
            mock_ds = mock_netcdf4.Dataset.return_value
            dataset = manager.__enter__()
            mock_netcdf4.Dataset.assert_called_with(fake_path, fake_mode,
                                                    format=fake_format)
            assert dataset is mock_ds

    def test_exit(self):
        fake_path = "no/path/here"
        fake_mode = "phony"
        fake_format = "fake"
        manager = netcdfcustomizer.NetcdfContextManager(
            fake_path, mode=fake_mode, format_=fake_format)
        with mock.patch("netcdf_cglops_tools.netcdfcustomizer.netCDF4",
                        autospec=True) as mock_netcdf4:
            mock_ds = mock_netcdf4.Dataset.return_value
            manager.__enter__()
            manager.__exit__(None, None, None)
            assert mock_ds.close.called


def test_get_cdo_manager_invalid_cdo_path():
    fake_cdo_path = "some_fake_path"
    os.environ["CDO_PATH"] = fake_cdo_path
    with pytest.raises(OSError):
        netcdfcustomizer.get_cdo_manager()


def test_get_cdo_manager_no_env():
    prefix = "netcdf_cglops_tools.netcdfcustomizer"
    with mock.patch(prefix + ".cdo",
                    autospec=True) as mock_cdo, \
            mock.patch(prefix + ".os", autospec=True) as mock_os:
        mock_os.getenv.return_value = None
        mock_Cdo = mock_cdo.Cdo.return_value
        result = netcdfcustomizer.get_cdo_manager()
        assert result == mock_Cdo
        mock_os.getenv.assert_called_with("CDO_PATH")
        assert not mock_Cdo.setCdo.called


def test_get_cdo_manager_with_env():
    fake_cdo_path = "something"
    prefix = "netcdf_cglops_tools.netcdfcustomizer"
    with mock.patch(prefix + ".cdo",
                    autospec=True) as mock_cdo, \
            mock.patch(prefix + ".os", autospec=True) as mock_os:
        mock_os.getenv.return_value = fake_cdo_path
        mock_Cdo = mock_cdo.Cdo.return_value
        result = netcdfcustomizer.get_cdo_manager()
        assert result == mock_Cdo
        mock_os.getenv.assert_called_with("CDO_PATH")
        mock_Cdo.setCdo.assert_called_with(fake_cdo_path)


def test_recreate_missing_attributes():
    fake_original = "fake"
    fake_new = "phony"
    fake_valid_range = "some fake value"
    fake_variable_name = "some variable"
    prefix = "netcdf_cglops_tools.netcdfcustomizer"
    with mock.patch(prefix + ".NetcdfContextManager",
                    autospec=True) as mock_ctx, \
            mock.patch(prefix + ".netcdfanalyzer.ProductInfo",
                       autospec=True) as mock_prod_info, \
            mock.patch(prefix + ".netcdfanalyzer.VariableInfo",
                       autospec=True) as var_info, \
            mock.patch(prefix + ".netCDF4.Dataset") as mock_ds, \
            mock.patch(prefix + ".netCDF4.Variable") as mock_variable:

        mocked_variable_info = var_info.return_value
        mocked_variable_info.valid_range = fake_valid_range
        mock_prod_info.return_value.variables = {
            fake_variable_name: mocked_variable_info
        }

        mocked_variable = mock_variable.return_value
        mocked_dataset = mock_ds.return_value
        mocked_dataset.variables = {
            fake_variable_name: mocked_variable
        }
        mock_ctx.return_value.__enter__.return_value = mocked_dataset

        netcdfcustomizer._recreate_missing_attributes(fake_original, fake_new)
        assert mocked_variable.valid_range == fake_valid_range


def test_create_netcdf_grid_mapping():
    fake_path = "/some/random/path"
    create_variable_args = ("crs", str)
    prefix = "netcdf_cglops_tools.netcdfcustomizer"
    with mock.patch(prefix + ".NetcdfContextManager",
                    autospec=True) as mock_ctx, \
            mock.patch(prefix + ".netCDF4",
                       autospec=True) as mock_netcdf4, \
            mock.patch(prefix + ".netCDF4.Variable",
                       autospec=True) as mock_variable:
        mock_ds = mock_netcdf4.Dataset.return_value
        mocked_variable = mock_variable.return_value
        mocked_variable.grid_mapping = mock.PropertyMock()
        mocked_variable.name = "fake variable"
        mock_ds.variables = {
            mocked_variable.name: mocked_variable
        }
        mock_grid_mapping = mock_ds.createVariable.return_value
        mocked_context = mock_ctx.return_value
        mocked_context.__enter__.return_value = mock_ds
        netcdfcustomizer._create_netcdf_grid_mapping(fake_path)
        mock_ctx.assert_called_with(fake_path, "a")
        mock_ds.createVariable.assert_called_with(*create_variable_args)
        assert mock_grid_mapping.grid_mapping_name == "latitude_longitude"
        assert mock_grid_mapping.longitude_of_prime_meridian == 0.0
        assert mock_grid_mapping.semi_major_axis == 6378137.0
        assert mock_grid_mapping.inverse_flattening == 298.257223563


@pytest.mark.parametrize("input_value,input_lower,input_upper,expected", [
    (100, "W", "E", "E100.00"),
    (-100, "W", "E", "W100.00"),
    (30, "S", "N", "N30.00"),
    (-30, "S", "N", "S30.00"),
])
def test_format_coordinate(input_value, input_lower, input_upper, expected):
    result = netcdfcustomizer._format_coordinate(
        input_value, input_lower, input_upper)
    assert result == expected


@pytest.mark.parametrize("path,extra,out,expected", [
    (
        "relative_path.nc",
        ["stuff"],
        "relative_dir",
        "relative_dir/relative_path_stuff.nc"
    ),
    (
        "/some/fixed/path/filename_big.nc",
        ["more", "stuff"],
        "/absolute/dir",
        "/absolute/dir/filename_big_more_stuff.nc"
    )
])
def test_get_output_path(path, extra, out, expected):
    result = netcdfcustomizer._get_output_path(path, extra, output_dir=out)
    assert result == expected


@pytest.mark.parametrize(
    "flon,flat,llon,llat,padding,exflon,exflat,exllon,exllat", [
        (-5, -10, 20, 30, 5, -10, -15, 25, 35),
        (0, 0, 0, 0, 1, -1, -1, 1, 1),
    ]
)
def test_correct_bounds(flon, flat, llon, llat, padding, exflon, exflat,
                        exllon, exllat):
    corrected = netcdfcustomizer._correct_bounds(flon, flat, llon, llat,
                                                 padding)
    assert corrected[0] == exflon
    assert corrected[1] == exflat
    assert corrected[2] == exllon
    assert corrected[3] == exllat


def test_extract_region_of_interest_auto_padding():
    first_lon = -10
    last_lon = -5
    first_lat = 30
    last_lat = 40
    corrected_flon = 1
    corrected_flat = 2
    corrected_llon = 3
    corrected_llat = 4
    fake_resolution = 33
    fake_path = "some/fake/path.nc"
    fake_output_path = "some/output/path"
    prefix = "netcdf_cglops_tools.netcdfcustomizer"
    with mock.patch(prefix + ".get_cdo_manager",
                    autospec=True) as mock_get_manager, \
            mock.patch(prefix + "._create_netcdf_grid_mapping",
                       autospec=True) as mock_create_grid, \
            mock.patch(prefix + "._get_output_path",
                       autospec=True) as mock_get_output_path, \
            mock.patch(prefix + "._recreate_missing_attributes",
                       autospec=True) as mock_recreate, \
            mock.patch(prefix + "._correct_bounds",
                       autospec=True) as mock_correct, \
            mock.patch("netcdf_cglops_tools.netcdfanalyzer.ProductInfo",
                       autospec=True) as mock_ProductInfo:
        mock_correct.return_value = (
            corrected_flon, corrected_flat, corrected_llon, corrected_llat)
        mock_ProductInfo.return_value.resolution = fake_resolution
        mock_Cdo = mock.MagicMock()
        mock_Cdo.sellonlatbox.return_value = True
        mock_get_manager.return_value = mock_Cdo
        mock_get_output_path.return_value = fake_output_path
        result = netcdfcustomizer.extract_region_of_interest(
            fake_path, first_lon, first_lat, last_lon, last_lat)
        mock_Cdo.sellonlatbox.assert_called_with(
            "{},{},{},{}".format(str(corrected_flon), str(corrected_llon),
                                 str(corrected_flat), str(corrected_llat)),
            input=str(fake_path),
            output=fake_output_path,
            options="-z zip_9"
        )
        mock_recreate.assert_called_with(fake_path, fake_output_path)
        mock_correct.assert_called_with(first_lon, first_lat, last_lon,
                                        last_lat, fake_resolution)
        assert result == fake_output_path


def test_extract_region_of_interest_manual_padding():
    first_lon = -10
    last_lon = -5
    first_lat = 30
    last_lat = 40
    corrected_flon = 1
    corrected_flat = 2
    corrected_llon = 3
    corrected_llat = 4
    fake_padding = -54
    fake_path = "some/fake/path.nc"
    fake_output_path = "some/output/path"
    prefix = "netcdf_cglops_tools.netcdfcustomizer"
    with mock.patch(prefix + ".get_cdo_manager",
                    autospec=True) as mock_get_manager, \
            mock.patch(prefix + "._create_netcdf_grid_mapping",
                       autospec=True) as mock_create_grid, \
            mock.patch(prefix + "._get_output_path",
                       autospec=True) as mock_get_output_path, \
            mock.patch(prefix + "._recreate_missing_attributes",
                       autospec=True) as mock_recreate, \
            mock.patch(prefix + "._correct_bounds",
                       autospec=True) as mock_correct:
        mock_correct.return_value = (
            corrected_flon, corrected_flat, corrected_llon, corrected_llat)
        mock_Cdo = mock.MagicMock()
        mock_Cdo.sellonlatbox.return_value = True
        mock_get_manager.return_value = mock_Cdo
        mock_get_output_path.return_value = fake_output_path
        result = netcdfcustomizer.extract_region_of_interest(
            fake_path, first_lon, first_lat, last_lon, last_lat,
            padding=fake_padding
        )
        mock_Cdo.sellonlatbox.assert_called_with(
            "{},{},{},{}".format(str(corrected_flon), str(corrected_llon),
                                 str(corrected_flat), str(corrected_llat)),
            input=str(fake_path),
            output=fake_output_path,
            options="-z zip_9"
        )
        mock_recreate.assert_called_with(fake_path, fake_output_path)
        mock_correct.assert_called_with(first_lon, first_lat, last_lon,
                                        last_lat, fake_padding)
        assert result == fake_output_path

def test_extract_variables():
    fake_path = "some/fake/path"
    fake_variables = ["var1", "var2"]
    fake_output_dir = "something"
    fake_output_path = "whatever"
    prefix = "netcdf_cglops_tools.netcdfcustomizer"
    with mock.patch(prefix + "._get_output_path",
                    autospec=True) as mock_get_output_path, \
            mock.patch(prefix + ".get_cdo_manager",
                       autospec=True) as mock_get_manager, \
            mock.patch(prefix + "._create_netcdf_grid_mapping",
                       autospec=True) as mock_create_grid, \
            mock.patch(prefix + "._recreate_missing_attributes",
                       autospec=True) as mock_recreate:
        mock_get_output_path.return_value = fake_output_path
        mock_Cdo = mock.MagicMock()
        mock_get_manager.return_value = mock_Cdo
        result = netcdfcustomizer.extract_variables(fake_path, fake_variables,
                                                    output_dir=fake_output_dir)
        assert result == fake_output_path
        mock_get_output_path.assert_called_with(fake_path, fake_variables,
                                                output_dir=fake_output_dir)
        mock_Cdo.selname.assert_called_with(
            "{},{}".format(fake_variables[0], fake_variables[1]),
            input=fake_path,
            output=fake_output_path,
            options="-z zip_9"
        )
        mock_create_grid.assert_called_with(fake_output_path)
        mock_recreate.assert_called_with(fake_path, fake_output_path)


def test_customize_product_no_customization():
    fake_path = "some/fake/path"
    prefix = "netcdf_cglops_tools.netcdfcustomizer"
    with mock.patch(prefix + ".extract_variables",
                    autospec=True) as mock_extract_variables, \
            mock.patch(prefix + ".extract_region_of_interest",
                       autospec=True) as mock_extract_roi:
        result = netcdfcustomizer.customize_product(fake_path)
        assert result == fake_path
        assert not mock_extract_variables.called
        assert not mock_extract_roi.called


def test_customize_product_with_extract_variables():
    fake_path = "some/fake/path"
    fake_variables = ["phony"]
    fake_output_dir = "nothing"
    expected_result = "stuff"
    prefix = "netcdf_cglops_tools.netcdfcustomizer"
    with mock.patch(prefix + ".extract_variables",
                    autospec=True) as mock_extract_variables, \
            mock.patch(prefix + ".extract_region_of_interest",
                       autospec=True) as mock_extract_roi:
        mock_extract_variables.return_value = expected_result
        result = netcdfcustomizer.customize_product(
            fake_path, variables=fake_variables, output_dir=fake_output_dir)
        assert result == expected_result
        assert not mock_extract_roi.called
        mock_extract_variables.assert_called_with(
            fake_path, fake_variables,
            output_dir=fake_output_dir,
            internal_compression=9
        )


def test_customize_product_with_extract_region_of_interest():
    fake_path = "some/fake/path"
    fake_roi = netcdfcustomizer.CdoLonLatBox(
        first_lon=-10, last_lon=10, first_lat=-20, last_lat=20)
    fake_output_dir = "nothing"
    expected_result = "stuff"
    prefix = "netcdf_cglops_tools.netcdfcustomizer"
    with mock.patch(prefix + ".extract_variables",
                    autospec=True) as mock_extract_variables, \
            mock.patch(prefix + ".extract_region_of_interest",
                       autospec=True) as mock_extract_roi:
        mock_extract_roi.return_value = expected_result
        result = netcdfcustomizer.customize_product(
            fake_path,
            region_of_interest=fake_roi,
            output_dir=fake_output_dir
        )
        assert result == expected_result
        assert not mock_extract_variables.called
        mock_extract_roi.assert_called_with(
            fake_path, fake_roi.first_lon, fake_roi.first_lat,
            fake_roi.last_lon, fake_roi.last_lat,
            output_dir=fake_output_dir,
            internal_compression=9
        )


def test_customize_with_extract_roi_extract_variables():
    fake_path = "some/fake/path"
    fake_roi = netcdfcustomizer.CdoLonLatBox(
        first_lon=-10, last_lon=10, first_lat=-20, last_lat=20)
    fake_variables = ["phony"]
    fake_output_dir = "nothing"
    expected_intermediate = "other"
    expected_result = "stuff"
    prefix = "netcdf_cglops_tools.netcdfcustomizer"
    with mock.patch(prefix + ".extract_variables",
                    autospec=True) as mock_extract_variables, \
            mock.patch(prefix + ".extract_region_of_interest",
                       autospec=True) as mock_extract_roi, \
            mock.patch(prefix + ".os", autospec=True) as mock_os:
        mock_extract_variables.return_value = expected_intermediate
        mock_extract_roi.return_value = expected_result
        result = netcdfcustomizer.customize_product(
            fake_path,
            variables=fake_variables,
            region_of_interest=fake_roi,
            output_dir=fake_output_dir
        )
        assert result == expected_result
        mock_extract_variables.assert_called_with(
            fake_path, fake_variables, output_dir=fake_output_dir,
            internal_compression=9
        )
        mock_extract_roi.assert_called_with(
            expected_intermediate, fake_roi.first_lon, fake_roi.first_lat,
            fake_roi.last_lon, fake_roi.last_lat,
            output_dir=fake_output_dir,
            internal_compression=9
        )
        mock_os.remove.assert_called_with(expected_intermediate)
