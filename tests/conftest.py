"""pytest configuration file."""

import os

import pytest


def pytest_configure(config):
    config.addinivalue_line(
        "markers",
        "unit: run only unit tests"
    )
    config.addinivalue_line(
        "markers",
        "functional: run only functional tests"
    )


def pytest_addoption(parser):
    parser.addoption(
        "--lst-nc-path",
        help="Full path to a netCDF file to use for functional tests that "
             "need LST products"
    )
    parser.addoption(
        "--lst10-nc-path",
        help="Full path to a netCDF file to use for functional tests that "
             "need LST10 Daily Cycle products"
    )


@pytest.fixture
def lst_nc_path(request):
    """Path to a netCDF file with an LST product."""
    return request.config.getoption("--lst-nc-path")


@pytest.fixture
def lst10_nc_path(request):
    """Path to a netCDF file with an LST10 Daily Cycle product."""
    return request.config.getoption("--lst10-nc-path")
