#!/usr/bin/env python

import io
from glob import glob
from os.path import basename
from os.path import dirname
from os.path import join
from os.path import splitext

from setuptools import setup
from setuptools import find_packages


def read(*names, **kwargs):
    return io.open(
        join(dirname(__file__), *names),
        encoding=kwargs.get("encoding", "utf-8")
    ).read()


setup(
    name="netcdf_cglops_tools",
    version=read("VERSION"),
    description="Tools for working with IPMA's CGLOPS netcdf products",
    long_description=read("README.rst"),
    author="Ricardo Garcia Silva",
    author_email="ricardo.silva@ipma.pt",
    url="http://gitlab.com/giosystem/netcdf-cglops-tools",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "Topic :: Software Development :: Libraries",
        "Programming Language :: Python :: 2",
    ],
    platforms=[""],
    license="Apache license",
    packages=find_packages(where="src"),
    package_dir={"": "src"},
    py_modules=[
        splitext(basename(path))[0] for path in glob("src/*.py")],
    include_package_data=True,
    install_requires=[
        "cdo",
        "netCDF4",
        "pystache",
    ]
)
