"""
Extract relevant information from a NetCDF file.

"""

from __future__ import division
import re
import datetime as dt
from collections import namedtuple
import logging

import netCDF4

logger = logging.getLogger(__name__)

ContactDetails = namedtuple(
    "ContactDetails",
    [
        "name",
        "short_name",
        "position",
        "delivery_point",
        "city",
        "postal_code",
        "country",
        "country_code",
        "e_mail",
        "website",
        "role",
    ]
)

BoundingBox = namedtuple(
    "BoundingBox",
    [
        "upper_left_lon",
        "upper_left_lat",
        "lower_right_lon",
        "lower_right_lat",
    ]
)


class VariableInfo(object):

    def __init__(self, nc_variable, file_path):
        self.name = nc_variable.name
        try:
            self.ancillary_variables = [
                i.strip() for i in nc_variable.ancillary_variables.split(",")]
        except AttributeError:
            self.ancillary_variables = []

        # TODO: LST10's variables do not have a `comment` attribute yet, check issue #622
        self.comment = getattr(nc_variable, "comment", "")
        self.flag_meanings = getattr(nc_variable, "flag_meanings", None)
        self.flag_masks = getattr(nc_variable, "flag_masks", None)
        self.flag_values = getattr(nc_variable, "flag_values", None)
        self.standard_name = getattr(nc_variable, "standard_name", None)
        self.cell_methods = getattr(nc_variable, "cell_methods", None)
        self.add_offset = getattr(nc_variable, "add_offset", 0)
        self.units = getattr(nc_variable, "units", None)
        self.scale_factor = getattr(nc_variable, "scale_factor", 1)
        self.fill_value = nc_variable._FillValue
        self.long_name = nc_variable.long_name
        self.shape = nc_variable.shape
        self.valid_range = nc_variable.valid_range
        self.dtype = nc_variable.dtype
        self.gdal_path = "NETCDF:{}:{}".format(file_path, self.name)


class ProductInfo(object):

    def __init__(self, nc_path):
        with netCDF4.Dataset(nc_path) as ds:
            self.name = ds.name
            self.timeslot = self.extract_timeslot(ds.variables["time"])
            self.gdal_path = nc_path
            self.long_name = ds.long_name
            self.title = ds.title
            self.identifier = ds.identifier
            self.parent_identifier = ds.parent_identifier
            self.comment = ds.comment
            self.purpose = ds.purpose
            self.date_created = dt.datetime.strptime(ds.date_created,
                                                     "%Y-%m-%dT%H:%M:%SZ")
            self.Conventions = ds.Conventions
            self.product_version = ds.product_version
            self.processing_mode = ds.processing_mode
            self.references = {name: url for name, url in [
                line.split(":", 1) for line in ds.references.splitlines()]}
            self.platform = [i.strip() for i in ds.platform.split(",")]
            self.source = ds.source
            self.archive_facility = ds.archive_facility
            self.sensor = [i.strip() for i in ds.sensor.split(",")]
            self.processing_level = ds.processing_level
            self.inspire_theme = ds.inspire_theme
            self.time_coverage_start = dt.datetime.strptime(
                ds.time_coverage_start, "%Y-%m-%dT%H:%M:%SZ")
            self.time_coverage_end = dt.datetime.strptime(
                ds.time_coverage_end, "%Y-%m-%dT%H:%M:%SZ")
            self.institution = ds.institution
            self.contacts = self.extract_contact_details(ds.contacts)
            self.algorithm_version = [i.strip() for i in
                                      ds.algorithm_version.split(",")]
            self.gemet_keywords = [i.strip() for i in
                                   ds.gemet_keywords.split(",")]
            self.gcmd_keywords = [i.strip() for i in
                                  ds.gcmd_keywords.split(",")]
            self.iso19115_topic_categories = [
                i.strip() for i in ds.iso19115_topic_categories.split(",")]
            self.other_keywords = [i.strip() for i in
                                   ds.other_keywords.split(",")]
            self.credit = ds.credit
            self.orbit_type = ds.orbit_type
            self.history = [i.strip() for i in ds.history.split(";")]
            bounding_box, resolution = self.extract_geographic_bounds(ds)
            self.bounding_box = bounding_box
            self.edge_bounding_box = self.extract_edge_bounding_box(ds)
            self.resolution = resolution
            self.x_size = ds.variables["lon"].size
            self.y_size = ds.variables["lat"].size
            self.variables = {}
            for ds_var in (v for v in ds.variables.values() if
                           v.name not in ("crs", "lat", "lon", "time")):
                self.variables[ds_var.name] = VariableInfo(ds_var,
                                                           self.gdal_path)

    @classmethod
    def extract_geographic_bounds(cls, dataset):
        """Get the geographic bounds of the input dataset.

        This method returns the coordinates of the center of the pixels

        Parameters
        ----------
        dataset: netCDF4.Dataset
            NetCDF dataset to analyze

        Returns
        -------
        bb: BoundingBox
            The geographic bounds of the input dataset
        x_resolution: float
            The spatial resolution

        """

        lat = dataset.variables["lat"]
        lon = dataset.variables["lon"]
        y_resolution = abs(lat[1] - lat[0])
        x_resolution = abs(lon[1] - lon[0])
        # we are considering the center of the pixel for these values
        # that is the reason for dividing resolution by two
        upper_y = lat[0] - y_resolution / 2
        lower_y = lat[-1] - y_resolution / 2
        left_x = lon[0] + x_resolution / 2
        right_x = lon[-1] + x_resolution / 2

        bb = BoundingBox(upper_left_lon=left_x,
                         upper_left_lat=upper_y,
                         lower_right_lon=right_x,
                         lower_right_lat=lower_y)
        return bb, x_resolution

    @classmethod
    def extract_edge_bounding_box(cls, dataset):
        """Get the geographic bounds of the input dataset.

        This method returns the coordinates of the edges of the pixels

        Parameters
        ----------
        dataset: netCDF4.Dataset
            NetCDF dataset to analyze

        Returns
        -------
        BoundingBox
            Geographic bounds of the input dataset

        """

        lat = dataset.variables["lat"]
        lon = dataset.variables["lon"]
        x_resolution = abs(lat[1] - lat[0])
        y_resolution = abs(lon[1] - lon[0])
        upper_y = lat[0]
        lower_y = lat[-1] - y_resolution
        left_x = lon[0]
        right_x = lon[-1] + x_resolution
        return BoundingBox(upper_left_lon=left_x,
                           upper_left_lat=upper_y,
                           lower_right_lon=right_x,
                           lower_right_lat=lower_y)

    @classmethod
    def extract_contact_details(cls, contact_expression):
        contacts = []
        for contact_line in contact_expression.splitlines():
            rest, sep, website = contact_line.rpartition(";")
            pattern = (
                r"(?P<role>[\w ]+)\(?(?P<position>[\w -]*)\)?: "
                r"(?P<email>[\w.@-]+); (?P<name>[\w -]+) \("
                r"(?P<short_name>[\w-]+)\); (?P<delivery_point>[\w -',.]+); "
                r"(?P<city>[\w ]+); (?P<postal_code>[\w-]+); "
                r"(?P<country>\w+) \((?P<country_code>\w+)\);"
            )
            try:
                found = re.search(pattern, rest,
                                  flags=re.UNICODE).groupdict()
            except AttributeError:
                logger.error("Could not extract contact details "
                             "from: {0}".format(contact_line))
                continue
            else:
                role = found["role"].strip()
                position = (
                    found["position"] if found["position"] != "" else role)
                contact_details = ContactDetails(
                    name=found["name"],
                    short_name=found["short_name"],
                    position=position,
                    delivery_point=found["delivery_point"],
                    city=found["city"],
                    postal_code=found["postal_code"],
                    country=found["country"],
                    country_code=found["country_code"],
                    e_mail=found["email"],
                    website=website.strip(),
                    role=role,
                )
                contacts.append(contact_details)
        return contacts

    @classmethod
    def extract_timeslot(cls, time_variable, step=0):
        info = time_variable.units.split(" since ")
        base_dt = dt.datetime.strptime(info[1], "%Y-%m-%d %H:%M:%S")
        unit_map = {
            "days": dt.timedelta(days=step),
            "hours": dt.timedelta(hours=step),
            "minutes": dt.timedelta(minutes=step),
            "seconds": dt.timedelta(seconds=step),
        }
        return base_dt + unit_map[info[0]]


