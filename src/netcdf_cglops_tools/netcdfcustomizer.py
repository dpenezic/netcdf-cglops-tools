"""Customize NetCDF products."""

from __future__ import absolute_import
from collections import namedtuple
import os
import logging
import re

import netCDF4
import cdo

from . import netcdfanalyzer

logger = logging.getLogger(__name__)

CdoLonLatBox = namedtuple("CdoLonLatBox",
                          ["first_lon", "last_lon", "first_lat", "last_lat"])


def get_cdo_manager():
    """Get the CDO wrapper

    If the environment variable ``CDO_PATH`` is defined, it will be used
    as the path to the CDO binary

    """

    cdo_manager = cdo.Cdo()
    cdo_path = os.getenv("CDO_PATH")
    if cdo_path is not None:
        cdo_manager.setCdo(cdo_path)
    return cdo_manager


def customize_product(nc_path, variables=None, region_of_interest=None,
                      output_dir=None, internal_compression=9):
    """Return a new customized NetCDF product.

    This function uses CDO to perform customization of a product's geographic
    region and to extract netCDF variables.

    Parameters
    ----------

    nc_path: str
        Full path to the netCDF product that will be customized.
    variables: list, optional
        an iterable with the names of the netCDF variables to
        extract from the input product.
    region_of_interest: CdoLonLatBox, optional
        A pair of latitude and longitude
        coordinates for a region of interest to extract from the input product.
    output_dir: str, optional
        The full path to the directory where the output customized file will
        be created.
    internal_compression: int, optional
        The level of internal compression to use for the output file.

    Returns
    -------
    str
        The full path to the customized file

    Example usage:

    >>> roi = CdoLonLatBox(first_lon=-3.36, last_lon=2.51,
    ...                    first_lat=2.65, last_lat=22.21)
    >>> variables = ["LST", "Q_FLAGS"]
    >>> original_product = ("/home/geo2/Desktop/cdo_tests/input/"
    ...                     "g2_BIOPAR_LST_201512200800_GLOBE_GEO_V1.2.nc")
    >>> output_dir = "/home/geo2/Desktop/cdo_tests/automated"
    >>> cdo_manager = cdo.Cdo()
    >>> cdo_manager.setCdo("/home/geo2/programas/bin/cdo")
    >>> no_options = customize_product(original_product,
    ...                                cdo_manager=cdo_manager)
    >>> only_variables = customize_product(original_product,
    ...                                    variables=variables,
    ...                                    cdo_manager=cdo_manager,
    ...                                    output_dir=output_dir)
    >>> only_roi = customize_product(original_product, region_of_interest=roi,
    ...                              cdo_manager=cdo_manager,
    ...                              output_dir=output_dir)
    >>> both = customize_product(original_product, variables=variables,
    ...                          region_of_interest=roi,
    ...                          cdo_manager=cdo_manager,
    ...                          output_dir=output_dir)

    """

    result = nc_path
    custom_variables = None
    if variables:
        custom_variables = extract_variables(
                nc_path, variables,
                output_dir=output_dir,
                internal_compression=internal_compression,
        )
        result = custom_variables
    if region_of_interest:
        custom_roi = extract_region_of_interest(
                result, region_of_interest.first_lon,
                region_of_interest.first_lat, region_of_interest.last_lon,
                region_of_interest.last_lat,
                output_dir=output_dir,
                internal_compression=internal_compression,
        )
        result = custom_roi
    if variables and region_of_interest:
        # delete the intermediate file with the extracted variables
        logger.debug("Deleting {}...".format(custom_variables))
        os.remove(custom_variables)
    return result


def extract_variables(nc_path, variables, output_dir=None,
                      internal_compression=9):
    """Generate a new NetCDF stripping away certain variables.

    This method will remove the NetCDF variables that are not mentioned as
    inputs. It uses the CDO 'selname' operator.

    Parameters
    ----------
    nc_path: str
        Original NetCDF file to be processed.
    variables: list
        An iterable with the names of the variables to keep.
    output_dir: str
        Directory where the output NetCDF will be stored
    internal_compression: int
        Which value to use for internal compression of the output NetCDF file.
        This value is passed to CDO.

    Returns
    -------
    str
        Path to the newly generated NetCDF file.

    """

    output_path = _get_output_path(nc_path, variables, output_dir=output_dir)
    cdo_manager = get_cdo_manager()
    cdo_manager.selname(
            ",".join(variables),
            input=str(nc_path),  # cdo does not seem to like unicode strings
            output=output_path,
            options="-z zip_{}".format(internal_compression)
    )
    # we need to add back the grid_mapping, because CDO removes it
    logger.debug("Adding grid_mapping...")
    _create_netcdf_grid_mapping(output_path)
    # we also need to add back the valid_range attribute
    _recreate_missing_attributes(nc_path, output_path)
    return output_path


def extract_region_of_interest(nc_path, first_lon, first_lat, last_lon,
                               last_lat, output_dir=None,
                               internal_compression=9, padding=None):
    """Generate a new NetCDF with the input region of interest.

    Crops the input NetCDF path according to the input region of interest.

    Parameters
    ----------
    nc_path: str
        Original NetCDF file to be cropped.
    first_lon: float
        Longitude of the leftmost pixel to keep
    first_lat: float
        Latitude of the lowermost pixel
    last_lon: float
        Longitude of the rightmost pixel
    last_lat: float
        Latitude of the uppermost pixel
    output_dir: str
        Directory where the output NetCDF will be stored
    internal_compression: int
        Which value to use for internal compression of the output NetCDF file.
        This value is passed to CDO.
    padding: float
        Padding for the geographic coordinates, in order to ensure that the
        final output contains the full bounding box given. A value of 'None'
        will use the resolution of the input NetCDf path as the padding value.

    Returns
    -------
    str
        Path to the newly generated NetCDF file that is the result of cropping
        the input with the region of interest.

    """

    if padding is None:  # use the resolution of the input nc_path as padding
        info = netcdfanalyzer.ProductInfo(nc_path)
        padding = info.resolution
    corrected_bounds = _correct_bounds(first_lon, first_lat,
                                       last_lon, last_lat, padding)
    corrected_first_lon = corrected_bounds[0]
    corrected_first_lat = corrected_bounds[1]
    corrected_last_lon = corrected_bounds[2]
    corrected_last_lat = corrected_bounds[3]
    name_parts = [
        _format_coordinate(corrected_first_lon, "W", "E") +
        _format_coordinate(corrected_first_lat, "S", "N") +
        _format_coordinate(corrected_last_lon, "W", "E") +
        _format_coordinate(corrected_last_lat, "S", "N")
    ]
    output_path = _get_output_path(nc_path, name_parts, output_dir=output_dir)
    roi = "{},{},{},{}".format(str(corrected_first_lon),
                               str(corrected_last_lon),
                               str(corrected_first_lat),
                               str(corrected_last_lat))
    cdo_manager = get_cdo_manager()
    cdo_manager.sellonlatbox(
            roi,
            input=str(nc_path),  # cdo does not seem to like unicode strings
            output=output_path,
            options="-z zip_{}".format(internal_compression)
    )
    # we need to add back the grid_mapping, because CDO removes it
    logger.debug("Adding grid_mapping...")
    _create_netcdf_grid_mapping(output_path)
    # we also need to add back the valid_range attribute
    _recreate_missing_attributes(nc_path, output_path)
    return output_path


def _correct_bounds(first_lon, first_lat, last_lon, last_lat, padding):
    corrected_first_lon = first_lon - padding
    corrected_first_lat = first_lat - padding
    corrected_last_lon = last_lon + padding
    corrected_last_lat = last_lat + padding
    return (corrected_first_lon, corrected_first_lat,
            corrected_last_lon, corrected_last_lat)


def _get_output_path(input_path, extra_components, output_dir=None):
    """Create a proper output path for the processed file

    Parameters
    ----------
    input_path: str
        Path of the original file
    extra_components: list
        Any extra parts to add to the filename, that are added as a result of
        processing operations
    output_dir: str
        The final output directory for the processed file

    Returns
    -------
    str
        The output path to be used for the final file

    """

    in_dir, in_fname = os.path.split(input_path)
    base_name, extension = os.path.splitext(in_fname)
    number_pattern = r"\d+\.\d{2}"
    area_pattern = (r"(?P<first_lon>[EW]{0})"
                    r"(?P<first_lat>[NS]{0})"
                    r"(?P<last_lon>[EW]{0})"
                    r"(?P<last_lat>[NS]{0})".format(number_pattern))
    new_name_parts = base_name.split("_")
    for component in extra_components:
        is_roi = re.search(area_pattern, component)
        if is_roi:
            area_part_index = 4
            new_name_parts[area_part_index] = component
        else:
            new_name_parts.append(component)
    out_name = "_".join(new_name_parts) + extension
    out_dir = output_dir or in_dir
    out_path = os.path.join(out_dir, out_name)
    return out_path


def _format_coordinate(value, lower, upper):
    return "{}{:0.2f}".format(lower if value < 0 else upper, abs(value))


def _create_netcdf_grid_mapping(nc_path):
    """Add the CF-Conventions grid mapping to the input product."""
    with NetcdfContextManager(nc_path, "a") as dataset:
        grid_mapping = dataset.createVariable("crs", str)
        grid_mapping.grid_mapping_name = "latitude_longitude"
        # the following attributes may not be necessary for our case
        grid_mapping.longitude_of_prime_meridian = 0.0
        grid_mapping.semi_major_axis = 6378137.0
        grid_mapping.inverse_flattening = 298.257223563
        for variable in dataset.variables.values():
            if variable.name not in ("lat", "lon", "time"):
                variable.grid_mapping = grid_mapping.name


def _recreate_missing_attributes(original_path, new_path):
    """Recreate attributes that were present in the original NetCDF file.

    CDO does not transport some attributes from the input files to the outputs
    and we need to add them back.

    Parameters
    ----------
    original_path: str
        Path to the original NetCDF file that is being customized
    new_path: str
        Path to the resulting new file after processing

    """

    original_info = netcdfanalyzer.ProductInfo(original_path)
    with NetcdfContextManager(new_path, mode="a") as new_dataset:
        for variable_name, variable_info in original_info.variables.items():
            new_variable = new_dataset.variables.get(variable_name)
            if new_variable is not None:
                new_variable.valid_range = variable_info.valid_range


class NetcdfContextManager(object):

    def __init__(self, path, mode="r", format_="NETCDF4"):
        """A context manager for dealing with netCDF files

        Parameters
        ----------
        path: str
            Path to the netCDF file to use
        mode: str
            Mode for working with the file.
        format_:
            Specific netCDF format. Can be any of the formats supported by the
            netCDF4 package

        """

        self.path = path
        self.mode = mode
        self.format_ = format_
        self.dataset = None

    def __enter__(self):
        self.dataset = netCDF4.Dataset(self.path, self.mode,
                                       format=self.format_)
        return self.dataset

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.dataset.close()
